angular.module('CoreoKWS.controllers', []).
    controller("InstanceController", ['$scope', '$http', 'instanceService', function($scope, $http, instanceService) {
    	$scope.findAll = function(namespace) {
    	    instanceService.getInstances(namespace, function(err, instances) {
    		if (err) { console.log(err); }
    		$scope.instances = instances.items;
    	    });
    	}
    }]).
    controller("InstanceDetailController", ['$scope', '$stateParams', '$http', 'instanceService', function($scope, $stateParams, $http, instanceService) {
    	$scope.findInstance = function(namespace) {
	    var name = $stateParams.name;
	    console.log('detailing: ' + $stateParams.name);
    	    instanceService.findInstance(namespace, name, function(err, ins) {
    		if (err) { console.log(err); }
    		$scope.selectedInstance = ins;
    	    });
    	}	
    }]).
    controller("ReplicationController", ['$scope', '$http', 'replicationService', function($scope, $http, replicationService) {
    	$scope.findAll = function(namespace) {
    	    replicationService.getReplications(namespace, function(err, replications) {
    		if (err) { console.log(err); }
    		$scope.replications = replications.items;
    	    });
    	}
    }]).
    controller("ReplicationDetailController", ['$scope', '$stateParams', '$http', 'replicationService', function($scope, $stateParams, $http, replicationService) {
    	$scope.findReplication = function(namespace) {
	    var name = $stateParams.name;
	    console.log('detailing: ' + $stateParams.name);
    	    replicationService.findReplication(namespace, name, function(err, rep) {
    		if (err) { console.log(err); }
		console.log('selectedReplication: ' + JSON.stringify(rep));
    		$scope.selectedReplication = rep;
    	    });
    	}	
    }]).
    controller("SecurityGroupController", ['$scope', '$http', 'securityGroupService', function($scope, $http, securityGroupService) {
    	$scope.findAll = function(namespace) {
    	    securityGroupService.getSecurityGroups(namespace, function(err, securitygroups) {
    		if (err) { console.log(err); }
		console.log(securitygroups.node.nodes);
		var allGroups = [];
		for(var i = 0; i < securitygroups.node.nodes.length; i++){
		    var g = {};
		    g.name = securitygroups.node.nodes[i].key.split('/').pop();
		    g.rules = JSON.parse(securitygroups.node.nodes[i].value);
		    allGroups.push(g);
		}
    		$scope.securitygroups = allGroups;
    	    });
    	}
    }]).
    controller("SecurityGroupDetailController", ['$scope', '$stateParams', '$http', 'securityGroupService', function($scope, $stateParams, $http, securityGroupService) {
    	$scope.findSecurityGroup = function(namespace) {
	    var name = $stateParams.name;
	    console.log('detailing: ' + $stateParams.name);
    	    securityGroupService.findSecurityGroup(namespace, name, function(err, sg) {
    		if (err) { console.log(err); }
		console.log('selectedSecurityGroup: ' + JSON.stringify(sg));
		group = {};
		group.name = sg.node.key.split('/').pop();
		group.rules = sg.node.value;
    		$scope.selectedSecurityGroup = group;
    	    });
    	}	
    }]).
    controller("ImageController", ['$scope', '$http', 'imageService', function($scope, $http, imageService) {
    	$scope.findAll = function() {
    	    imageService.getImages(function(err, images) {
    		if (err) { console.log(err); }
    		$scope.images = images;
    	    });
    	}
    }]).
    controller("ImageDetailController", ['$scope', '$stateParams', '$http', 'imageService', function($scope, $stateParams, $http, imageService) {
	$scope.findImage = function(name) {
    	    imageService.findImage($stateParams.name, function(err, image) {
    		if (err) { console.log(err); }
		console.log('found image: ' + image);
		$scope.selectedImage = image;
    	    });
	}
    }])
