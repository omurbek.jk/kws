angular.module('CoreoKWS.services', []).
    factory('instanceService', ['$http', function($http) {
	var instanceAPI = {};
	instanceAPI.getInstances = function(namespace, callback) {
	    $http.get('/instances').success(function(instances){ 
		return callback(null, instances);
	    });
	};
	instanceAPI.findInstance = function(namespace, name, callback) {
	    $http.get('/instances/' + name).success(function(instance){ 
		return callback(null, instance);
	    });
	};
	return instanceAPI;
    }]).
    factory('replicationService', ['$http', function($http) {
	var replicationAPI = {};
	replicationAPI.getReplications = function(namespace, callback) {
	    $http.get('/replications').success(function(replications){ 
		return callback(null, replications);
	    });
	};
	replicationAPI.findReplication = function(namespace, name, callback) {
	    $http.get('/replications/' + name).success(function(replication){ 
		return callback(null, replication);
	    });
	};
	replicationAPI.scale = function(namespace, name, scale, callback) {
	    var data = {};
	    data.scale = scale;
	    $http.patch('/replications/' + name, data).success(function(replication){ 
		return callback(null, replication);
	    });
	};
	return replicationAPI;
    }]).
    factory('securityGroupService', ['$http', function($http) {
	var securityGroupAPI = {};
	securityGroupAPI.getSecurityGroups = function(namespace, callback) {
	    $http.get('/securitygroups').success(function(securityGroups){ 
		return callback(null, securityGroups);
	    });
	};
	securityGroupAPI.findSecurityGroup = function(namespace, name, callback) {
	    $http.get('/securitygroups/' + name).success(function(securityGroup){ 
		return callback(null, securityGroup);
	    });
	};
	return securityGroupAPI;
    }]).
    factory('imageService', ['$http', function($http) {
	var imageAPI = {};
	imageAPI.getImages = function(callback) {
	    $http.get('/images').success(function(images){ 
		return callback(null, images);
	    });
	};
	imageAPI.findImage = function(name, callback) {
	    $http.get('/images/' + name).success(function(image){ 
		return callback(null, image);
	    });
	};
	return imageAPI;
    }]).
    factory('nodeService', ['$http', function($http) {
	var nodeAPI = {};
	nodeAPI.getNodes = function(callback){
	    $http.get('/nodes').success(function(nodes) {
		if (err) { return callback(err, null); }
	    });
	};
	nodeAPI.findNode = function(name, callback){
	    $http.get('/nodes/' + name).success(function(node) {
		if (err) { return callback(err, null); }
	    });
	};
	return nodeAPI;
    }]);



