var coreoKWS = angular.module('coreoKWS'
			      ,[
				  ,'ui.router'
				  ,'CoreoKWS.services'
				  ,'CoreoKWS.controllers'
				  ,'CoreoKWS.filters'
			      ]
			     );

coreoKWS.config(function($stateProvider, $urlRouterProvider){
    $stateProvider
        .state('home', {
            url: '/home',
            templateUrl: 'partials/home.ejs'
        })
        .state('instances', {
            url: '/instances',
            templateUrl: 'templates/instances/instances.html'
        })
        .state('instances.detail', {
            url: '/:name',
            templateUrl: 'templates/instances/details.html',
	    controller: 'InstanceDetailController'
        })
        .state('images', {
            url: '/images',
            templateUrl: 'templates/images/images.html'
        })
        .state('images.detail', {
            url: '/:name',
            templateUrl: 'templates/images/details.html',
	    controller: 'ImageDetailController'
        })
        .state('replications', {
            url: '/replications',
            templateUrl: 'templates/replication/replication.html'
        })
        .state('replications.detail', {
            url: '/:name',
            templateUrl: 'templates/replication/details.html',
	    controller: 'ReplicationDetailController'
        })
        .state('securitygroups', {
            url: '/securitygroups',
            templateUrl: 'templates/securitygroups/securitygroups.html'
        })
        .state('securitygroups.detail', {
            url: '/:name',
            templateUrl: 'templates/securitygroups/details.html',
	    controller: 'SecurityGroupDetailController'
        })
});
