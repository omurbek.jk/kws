/**
 * ImagesController
 *
 * @description :: Server-side logic for managing images
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */
var helm = require('helmjs').helm({binary: sails.config.helm.binary});

module.exports = {
    find: function(req, res, next){
	helm.list(function(err, pkgSummaries){
	    var inserted = 0;
	    var allPackages = [];
	    for(var i = 0; i < pkgSummaries.length; i++) {
		helm.info(pkgSummaries[i].name, function(err, pkg){
		    if ( pkg.name && pkg.name.length > 0 ) { 
			allPackages.push(pkg);
		    }
		    if (++inserted == pkgSummaries.length) {
			return res.json(allPackages);
		    }
		});
	    }
	});
    },
    findOne: function(req, res, next){
	var params = req.params.all();
        sails.log.debug('instance findOne: ' + JSON.stringify(params));
	helm.info(params.id, function(err, pkg){
	    return res.json(pkg);
	});
    }
};

