/**
 * NodesController
 *
 * @description :: Server-side logic for managing instances
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */
var K8s = require('k8s');
var ep = sails.config.kube.protocol + '://' + sails.config.kube.kubeHost + ':' + sails.config.kube.port;
var kubectl = K8s.kubectl({
    endpoint: ep,
    binary: sails.config.kube.binary
});

module.exports = {
    find: function(req, res, next){
	kubectl.node.list(function(err, nodes){
	    return res.json(nodes);
	})
    },
    findOne: function(req, res, next){
	var params = req.params.all();
        sails.log.debug('nodes findOne: ' + JSON.stringify(params));
   	kubectl.node.get(params.id, function(err, nodes){
	    return res.json(nodes);
	})
    }
};

