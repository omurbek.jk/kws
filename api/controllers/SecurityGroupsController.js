/**
 * SecurityGroupsController
 *
 * @description :: Server-side logic for managing Securitygroups
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

Etcd = require('node-etcd');
var etcd = new Etcd(sails.config.etcd.etcdHost, sails.config.etcd.port);

module.exports = {
    find: function(req, res, next){
	etcd.get(sails.config.etcd.keyDir, { recursive: true }, function(err, groups){
	    if (err) { return res.status(500).json(err); }
	    return res.json(groups);
	})
    },
    findOne: function(req, res, next){
	var params = req.params.all();
	etcd.get(sails.config.etcd.keyDir + '/' + params.id, function(err, group){
	    return res.json(group);
	});
    }
};

