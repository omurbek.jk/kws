/**
 * ReplicationController
 *
 * @description :: Server-side logic for managing replications
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */
var K8s = require('k8s');
var ep = sails.config.kube.protocol + '://' + sails.config.kube.kubeHost + ':' + sails.config.kube.port;
var kubectl = K8s.kubectl({
    endpoint: ep,
    binary: sails.config.kube.binary
});

module.exports = {
    find: function(req, res, next){
	kubectl.rc.list(function(err, rcs){
	    if (err) { return res.status(500).json(err); }
	    return res.json(rcs);
	})
    },
    findOne: function(req, res, next){
	var params = req.params.all();
        sails.log.debug('replication findOne: ' + JSON.stringify(params));
	kubectl.rc.get(params.id, function(err, rcs){
	    return res.json(rcs);
	})
    },
    update: function(req, res, next){
	var params = req.params.all();
        sails.log.debug('replication update: ' + JSON.stringify(params));
	kubectl.rc.scale(params.name, params.scale, function(err, rcs){
	    return res.json(rcs);
	})
    }	
};

