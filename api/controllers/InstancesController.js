/**
 * InstanceController
 *
 * @description :: Server-side logic for managing instances
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */
var K8s = require('k8s');
var ep = sails.config.kube.protocol + '://' + sails.config.kube.kubeHost + ':' + sails.config.kube.port;
var kubectl = K8s.kubectl({
    endpoint: ep,
    binary: sails.config.kube.binary
});

module.exports = {
    find: function(req, res, next){
	kubectl.pod.list(function(err, pods){
	    if (err) { return res.status(500).json(err); }
	    return res.json(pods);
	})
    },
    findOne: function(req, res, next){
	var params = req.params.all();
        sails.log.debug('instance findOne: ' + JSON.stringify(params));
	kubectl.pod.get(params.id, function(err, pods){
	    return res.json(pods);
	})
    },
};

