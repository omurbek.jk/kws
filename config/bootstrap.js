/**
 * Bootstrap
 * (sails.config.bootstrap)
 *
 * An asynchronous bootstrap function that runs before your Sails app gets lifted.
 * This gives you an opportunity to set up your data model, run jobs, or perform some special logic.
 *
 * For more information on bootstrapping your app, check out:
 * http://sailsjs.org/#!/documentation/reference/sails.config/sails.config.bootstrap.html
 */
Etcd = require('node-etcd');

module.exports.bootstrap = function(cb) {
    var etcd = new Etcd(sails.config.etcd.etcdHost, sails.config.etcd.port);

    var defaultGroup = [
	{
	    'tcp': 22
	}
    ]
    //create some directories for storing things
    etcd.mkdir(sails.config.etcd.keyDir, { recursive: true }, console.log);
    etcd.set(sails.config.etcd.keyDir + '/default', JSON.stringify(defaultGroup), { maxRetries: 3 }, console.log);

    var helm = require('helmjs').helm({binary: sails.config.helm.binary});
    helm.update(function(err,data){
	console.log(data);
    });
  // It's very important to trigger this callback method when you are finished
  // with the bootstrap!  (otherwise your server will never lift, since it's waiting on the bootstrap)
    cb();
};
