module.exports.etcd = {
    etcdHost: 'dev-etcd.inplico.com',
    protocol: 'http',
    port: '2379',
    keyDir: 'kws/securitygroups'
};
