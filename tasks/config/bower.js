module.exports = function(grunt) {
  grunt.config.set('bower', {
    dev: {
        dest: '.tmp/public',
        js_dest: '.tmp/public/js',
        css_dest: '.tmp/public/styles',    
	"exportsOverride": {
	    "bootstrap": {
		"css": ["dist/css/bootstrap.css",
			"dist/css/bootstrap.css.map"],
		"js": "dist/js/bootstrap.js",
		"fonts": "dist/fonts"
	    }
	}
	
    }
  });

  grunt.loadNpmTasks('grunt-bower');

};
